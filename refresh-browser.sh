#!/bin/bash

focused_window_id=$(xdotool getwindowfocus)                             # remember current window
xdotool search --onlyvisible --class "Falkon" windowfocus key 'ctrl+r'  # send keystroke to refresh
xdotool windowactivate --sync $focused_window_id            
