#!/bin/bash

PID=""
Rscript run.R &
PID=$!
while true
do
    inotifywait -e modify problem-shiny.Rmd
    kill $PID 
    Rscript run.R &
    PID=$!
done
kill $PID 
