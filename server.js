const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8081 });

var udp = require('dgram');
var server = udp.createSocket("udp4")

var globalws = null;

server.on('listening', function(msg, info) {
  var addr = server.address();
  var port = addr.port;
  console.log("udp Server is listening on: " + port)
})
server.on('message', function(msg, info) {
    console.log("got udp reload message: " + msg)
    if(globalws) {
        console.log("sending websocket reload to browser")
        globalws.send("reload");
    } 
    else 
       console.log("not sending websocket reload")
   
})
server.bind(6000)

wss.on('connection', function connection(ws) {
  globalws=ws 
  console.log('connection: made');
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });
  ws.send("hello from server")
});
