---
server: shiny 
pagetitle: Problem
title: Interactive Problem
format:
  html:
    html-math-method: katex
output:
  html_document:
    math_method: katex
params:
  x:
    label: x
    value: 51
    input: slider
    min: 1
    max: 400
    step: 1
---


```{=html}
<script src="https://unpkg.com/vue@3"></script>
```

```{=html}
<style> .recalculating { opacity: 1.0 !important; } </style>
```

```{=html}
<style>  [v-cloak] { display: none !important; } </style>
```



:::{#problem v-cloak=""}


```{=html}
<div class="form-group shiny-input-container" style="width:100%;">
<label class="control-label" id="x-label" for="x"><span class="katex-display"><span class="katex"><span class="katex-mathml"><math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><semantics><mrow><mstyle mathsize="1.2em"><mi>x</mi></mstyle></mrow><annotation encoding="application/x-tex">\large{x}</annotation></semantics></math></span><span class="katex-html" aria-hidden="true"><span class="base"><span class="strut" style="height:0.5167em;"></span><span class="mord sizing reset-size6 size7"><span class="mord mathnormal">x</span></span></span></span></span></span></label>
<input class="js-range-slider" id="x" data-skin="shiny" data-min="1" data-max="400" data-from="40" data-step="1" data-grid="true" data-grid-num="9.975" data-grid-snap="false" data-prettify-separator="," data-prettify-enabled="true" data-keyboard="true" data-data-type="number" data-immediate="true"/>
</div>
```







Here is some text: 2(x)=2({{x}})={{w}}

Weee is some display. This is really cool...I hope it works $x = {{x}}$
for quarto as well. It does work for quarto...

$$
                            \huge{x = {{x}}}
$$

Since $w = 2(x)$, we have:

$$
                            \huge{w = {{w}}}
$$


```{=html}
<div id="plot" class="shiny-plot-output" style="width:100%;height:400px;"></div>
```


:::


<script type="text/javascript">
  function render_katex_math(){
        var mathElements = document.getElementsByClassName("math");
        var macros = [];
        for (var i = 0; i < mathElements.length; i++) {
            var numChildren=mathElements[i].childElementCount
            var texText = null;
            //only process it if its got no children...else its static latex.
            if(numChildren === 0) { 
              texText= mathElements[i].innerText;
              if (mathElements[i].tagName == "SPAN") {
                katex.render(texText, mathElements[i], {
                      displayMode: mathElements[i].classList.contains("display"),
                      throwOnError: false,
                      fleqn: false
                    });
               }
            }
         }
  }

function send_to_view(name, val) {
      if(vm !== null) {
        console.log("setting " + name + ": " + val) 
        vm.$data[[name]]=val
      }
  }

Shiny.addCustomMessageHandler('w',function(val){
    send_to_view('w', val)
});
Shiny.addCustomMessageHandler('x',function(val){
    send_to_view('x', val)
});

//.main-container for rmarkdown::run,  

$("#quarto-content").on("shiny:inputchanged", function(event) {
     console.log("shiny:inputchanged ") 
     send_to_view(event.name, event.value)
 });

//Need this global vue instance variable
var vm = null;

console.log("variable Vue is defined");
  var app= null;
  app=Vue.createApp({
  delimiters: ["{{","}}"],
    data() {
      return {
          x: null, w: null
      }
    },
    watch: {
        $data: {
                handler() {
                   console.log("in handler")
                   render_katex_math()
                },
                flush: "post",
                deep: true
            }
    },
    methods: {
        round: function(num, places) {
                 plusexpr="e+" + places
                 minusexpr="e-" + places
            return +(Math.round(num + plusexpr)  + minusexpr);
        }
    }
})

console.log("mounting view app")
vm=app.mount("#problem")
</script>
